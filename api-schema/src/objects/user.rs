use async_graphql::{Enum, Object};

#[derive(Enum, Copy, Clone, Eq, PartialEq, Debug)]
pub enum UserKind {
    Admin,
    User,
    Guest,
}

#[derive(Clone, Debug)]
pub struct User {
    id: Option<i32>,
    kind: UserKind,
    name: String,
}

#[Object]
impl User {
    async fn id(&self) -> Option<i32> {
        self.id
    }

    async fn kind(&self) -> UserKind {
        self.kind
    }

    async fn name(&self) -> &str {
        &self.name
    }
}
