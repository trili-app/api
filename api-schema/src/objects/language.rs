use async_graphql::Enum;

#[derive(Enum, Copy, Clone, Eq, PartialEq, Debug, PartialOrd, Ord)]
pub enum Language {
    English,
}

impl Language {
    pub fn abbreviation(&self) -> &str {
        match self {
            Language::English => "en",
        }
    }
}
