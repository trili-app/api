use super::language::Language;
use crate::sources::test_data::TestDataSource;

pub struct AppContext {
    test_data_source: TestDataSource,
    language: Language,
}

impl AppContext {
    pub fn new() -> Self {
        AppContext {
            test_data_source: TestDataSource::new(),
            language: Language::English,
        }
    }

    pub fn test_data_source(&self) -> &TestDataSource {
        &self.test_data_source
    }

    pub fn language(&self) -> &Language {
        &self.language
    }
}

impl Default for AppContext {
    fn default() -> Self {
        Self::new()
    }
}
