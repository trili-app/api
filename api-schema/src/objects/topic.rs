use async_graphql::{Context, Object, ID};

use super::{connection::Connection, context::AppContext, language::Language, template::Template};
use std::collections::BTreeMap;

#[derive(Clone, Debug)]
pub struct LocalizedTopic {
    id: String,
    topic_id: String,
    name: String,
    language: Language,
    connections: Vec<String>,
}

impl LocalizedTopic {
    pub fn new(
        id: &str,
        topic_id: &str,
        name: &str,
        language: Language,
        connections: &[String],
    ) -> Self {
        LocalizedTopic {
            id: String::from(id),
            topic_id: String::from(topic_id),
            language,
            name: String::from(name),
            connections: connections.to_owned(),
        }
    }

    pub fn id_internal(&self) -> &str {
        &self.id
    }

    pub fn name_internal(&self) -> &str {
        &self.name
    }

    pub fn language_internal(&self) -> &Language {
        &self.language
    }
}

#[Object]
impl LocalizedTopic {
    async fn id(&self) -> ID {
        ID::from(&self.id)
    }

    async fn name(&self) -> &str {
        &self.name
    }

    async fn language(&self) -> &Language {
        &self.language
    }

    async fn topic<'ctx>(&self, ctx: &Context<'ctx>) -> Topic {
        let app_context = ctx.data::<AppContext>().unwrap();
        let data_source = app_context.test_data_source();
        data_source.topic(&self.topic_id).unwrap()
    }

    async fn connections<'ctx>(&self, ctx: &Context<'ctx>) -> Vec<Connection> {
        let app_context = ctx.data::<AppContext>().unwrap();
        self.connections
            .iter()
            .flat_map(|conn_id| app_context.test_data_source().connection(conn_id))
            .collect()
    }
}

#[derive(Clone, Debug)]
pub struct Topic {
    id: String,
    languages: BTreeMap<Language, String>,
    template_id: String,
    connections: Vec<String>,
}

impl Topic {
    pub fn new(
        id: &str,
        template_id: &str,
        languages: &BTreeMap<Language, String>,
        connections: &[String],
    ) -> Self {
        Topic {
            id: String::from(id),
            languages: languages.clone(),
            template_id: String::from(template_id),
            connections: connections.to_owned(),
        }
    }

    pub fn id_internal(&self) -> &str {
        &self.id
    }

    pub fn languages(&self) -> &BTreeMap<Language, String> {
        &self.languages
    }

    pub fn template_id(&self) -> &str {
        &self.template_id
    }
}

#[Object]
impl Topic {
    async fn id(&self) -> ID {
        ID::from(&self.id)
    }

    // Always require english for now, later use english only as fallback.
    async fn content<'ctx>(&self, ctx: &Context<'ctx>) -> LocalizedTopic {
        let app_context = ctx.data::<AppContext>().unwrap();
        let data_source = app_context.test_data_source();

        self.languages
            .get(app_context.language())
            .and_then(|content_id| data_source.localized_topic(content_id))
            .unwrap()
    }

    async fn template<'ctx>(&self, ctx: &Context<'ctx>) -> Template {
        let app_context = ctx.data::<AppContext>().unwrap();
        let data_source = app_context.test_data_source();
        data_source.template(&self.template_id).unwrap()
    }

    async fn connections<'ctx>(&self, ctx: &Context<'ctx>) -> Vec<Connection> {
        let app_context = ctx.data::<AppContext>().unwrap();
        self.connections
            .iter()
            .flat_map(|conn_id| app_context.test_data_source().connection(conn_id))
            .collect()
    }
}
