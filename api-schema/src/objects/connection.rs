use super::{
    connection_schema::ConnectionSchema,
    contents::{date_content::DateContent, numeric_content::NumericContent, Content, ContentType},
    context::AppContext,
};
use async_graphql::{Context, Object, ID};
use chrono::{TimeZone, Utc};

#[derive(Clone, Debug)]
pub struct Connection {
    id: String,
    schema_id: String,
    content_id: String,
}

impl Connection {
    pub fn new(id: &str, schema_id: &str, content_id: &str) -> Self {
        Connection {
            id: String::from(id),
            schema_id: String::from(schema_id),
            content_id: String::from(content_id),
        }
    }

    pub fn id_internal(&self) -> &str {
        &self.id
    }
}

#[Object]
impl Connection {
    async fn id(&self) -> ID {
        ID::from(&self.id)
    }

    async fn schema<'ctx>(&self, ctx: &Context<'ctx>) -> ConnectionSchema {
        let app_context = ctx.data::<AppContext>().unwrap();
        app_context
            .test_data_source()
            .connection_schema(&self.schema_id)
            .unwrap()
    }

    async fn content<'ctx>(&self, ctx: &Context<'ctx>) -> Content {
        let app_context = ctx.data::<AppContext>().unwrap();
        let schema = app_context
            .test_data_source()
            .connection_schema(&self.schema_id)
            .unwrap();

        match schema.content_type() {
            ContentType::Date => {
                Content::DateContent(DateContent::new("123", &Utc.ymd(2021, 9, 5)))
            }
            ContentType::Markdown => Content::MarkdownContent(
                app_context
                    .test_data_source()
                    .leveled_markdown(&self.content_id)
                    .unwrap(),
            ),
            ContentType::Text => Content::TextContent(
                app_context
                    .test_data_source()
                    .text(&self.content_id, app_context.language())
                    .unwrap(),
            ),
            ContentType::Numeric => Content::NumericContent(NumericContent::new("123", 2.0)),
        }
    }
}
