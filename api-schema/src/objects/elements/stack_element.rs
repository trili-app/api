use super::element_reference::ElementReference;
use crate::objects::{
    context::AppContext,
    elements::{get_element_from_reference, Element, ElementType},
};
use async_graphql::{Context, Enum, Object, ID};

#[derive(Enum, Copy, Clone, Eq, PartialEq, Debug)]
pub enum Direction {
    Vertical,
    Horizontal,
}

#[derive(Clone, Debug)]
pub struct StackElement {
    id: String,
    direction: Direction,
    parent_id: Option<String>,
    children: Vec<ElementReference>,
}

impl StackElement {
    pub fn new(id: &str, parent_id: Option<String>) -> Self {
        StackElement {
            id: String::from(id),
            direction: Direction::Vertical,
            parent_id,
            children: Vec::new(),
        }
    }

    pub fn id_internal(&self) -> &str {
        &self.id
    }

    pub fn set_direction(&mut self, direction: Direction) {
        self.direction = direction;
    }

    pub fn add_child(&mut self, element: Element) {
        let e = match element {
            Element::HeaderElement(header) => {
                ElementReference::new(header.id_internal(), ElementType::Header)
            }
            Element::StackElement(stack) => {
                ElementReference::new(stack.id_internal(), ElementType::Stack)
            }
            Element::MarkdownElement(markdown) => {
                ElementReference::new(markdown.id_internal(), ElementType::Markdown)
            }
            Element::TableElement(_) => todo!(),
        };
        self.children.push(e);
    }

    pub fn children_internal(&self) -> &Vec<ElementReference> {
        &self.children
    }
}

#[Object]
impl StackElement {
    pub async fn id(&self) -> ID {
        ID::from(self.id_internal())
    }

    pub async fn parent_id(&self) -> Option<&str> {
        self.parent_id.as_deref()
    }

    pub async fn direction(&self) -> Direction {
        self.direction
    }

    async fn children<'ctx>(&self, ctx: &Context<'ctx>) -> Vec<Element> {
        let app_context = ctx.data::<AppContext>().unwrap();
        self.children
            .iter()
            .flat_map(|e| get_element_from_reference(e, app_context))
            .collect()
    }
}
