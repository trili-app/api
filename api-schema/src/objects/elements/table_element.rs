use async_graphql::{Object, ID};

#[derive(Clone, Debug)]
pub struct TableElement {
    id: String,
    parent_id: Option<String>,
}

impl TableElement {
    pub fn new(id: &str, parent_id: Option<String>) -> Self {
        TableElement {
            id: String::from(id),
            parent_id,
        }
    }

    pub fn id_internal(&self) -> &str {
        &self.id
    }
}

#[Object]
impl TableElement {
    pub async fn id(&self) -> ID {
        ID::from(self.id_internal())
    }

    pub async fn parent_id(&self) -> Option<&str> {
        self.parent_id.as_deref()
    }

    async fn headers(&self) -> Vec<String> {
        vec![String::from("header-a")]
    }
}
