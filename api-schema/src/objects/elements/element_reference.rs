use super::ElementType;

#[derive(Clone, Debug)]
pub struct ElementReference {
    element_id: String,
    element_type: ElementType,
}

impl ElementReference {
    pub fn new(element_id: &str, element_type: ElementType) -> Self {
        ElementReference {
            element_id: String::from(element_id),
            element_type,
        }
    }

    pub fn element_id(&self) -> &str {
        &self.element_id
    }

    pub fn element_type(&self) -> &ElementType {
        &self.element_type
    }
}
