use async_graphql::{Context, Object, ID};

use crate::objects::{contents::text_content::TextContent, context::AppContext};

#[derive(Clone, Debug)]
pub struct HeaderElement {
    id: String,
    parent_id: Option<String>,
    level: i32,
    text_identifier: String,
}

impl HeaderElement {
    pub fn new(id: &str, parent_id: Option<String>, text_identifier: &str, level: i32) -> Self {
        HeaderElement {
            id: String::from(id),
            parent_id,
            text_identifier: String::from(text_identifier),
            level,
        }
    }

    pub fn id_internal(&self) -> &str {
        &self.id
    }
}

#[Object]
impl HeaderElement {
    pub async fn id(&self) -> ID {
        ID::from(self.id_internal())
    }

    pub async fn parent_id(&self) -> Option<&str> {
        self.parent_id.as_deref()
    }

    pub async fn level(&self) -> i32 {
        self.level
    }

    pub async fn text_identifier(&self) -> &str {
        &self.text_identifier
    }

    pub async fn resolved_text<'ctx>(&self, ctx: &Context<'ctx>) -> Option<TextContent> {
        let app_context = ctx.data::<AppContext>().unwrap();
        app_context
            .test_data_source()
            .text(&self.text_identifier, app_context.language())
    }
}
