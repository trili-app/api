use self::{
    element_reference::ElementReference, header_element::HeaderElement,
    markdown_element::MarkdownElement, stack_element::StackElement, table_element::TableElement,
};
use crate::objects::context::AppContext;
use async_graphql::{Enum, Interface, ID};

pub mod element_reference;
pub mod header_element;
pub mod markdown_element;
pub mod stack_element;
pub mod table_element;

#[derive(Enum, Copy, Clone, Eq, PartialEq, Debug)]
pub enum ElementType {
    Header,
    Markdown,
    Table,
    Stack,
}

#[derive(Interface)]
#[graphql(
    field(name = "id", type = "ID"),
    field(name = "parent_id", type = "Option<&str>")
)]
pub enum Element {
    HeaderElement(HeaderElement),
    MarkdownElement(MarkdownElement),
    TableElement(TableElement),
    StackElement(StackElement),
}

impl Element {
    pub fn children(&self, ctx: &AppContext) -> Vec<Element> {
        self.get_elements_recursive(self, ctx)
    }

    fn get_elements_recursive(&self, element: &Element, ctx: &AppContext) -> Vec<Element> {
        match element {
            Element::StackElement(stack) => {
                let mut children = vec![Element::StackElement(stack.clone())];
                children.append(
                    &mut stack
                        .children_internal()
                        .iter()
                        .flat_map(|e| {
                            get_element_from_reference(e, ctx)
                                .map(|elem| self.get_elements_recursive(&elem, ctx))
                                .unwrap_or_default()
                        })
                        .collect(),
                );
                children
            }
            Element::HeaderElement(header) => vec![Element::HeaderElement(header.clone())],
            Element::MarkdownElement(markdown) => vec![Element::MarkdownElement(markdown.clone())],
            Element::TableElement(table) => vec![Element::TableElement(table.clone())],
        }
    }
}

pub fn get_element_from_reference(e: &ElementReference, ctx: &AppContext) -> Option<Element> {
    match e.element_type() {
        ElementType::Header => ctx
            .test_data_source()
            .header_element(e.element_id())
            .map(Element::HeaderElement),
        ElementType::Stack => ctx
            .test_data_source()
            .stack_element(e.element_id())
            .map(Element::StackElement),
        ElementType::Markdown => ctx
            .test_data_source()
            .markdown_element(e.element_id())
            .map(Element::MarkdownElement),
        ElementType::Table => todo!(),
    }
}
