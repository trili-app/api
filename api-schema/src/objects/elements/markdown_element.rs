use async_graphql::{Object, ID};

#[derive(Clone, Debug)]
pub struct MarkdownElement {
    id: String,
    markdown_identifier: String,
    parent_id: Option<String>,
}

impl MarkdownElement {
    pub fn new(id: &str, parent_id: Option<String>, markdown_identifier: &str) -> Self {
        MarkdownElement {
            id: String::from(id),
            parent_id,
            markdown_identifier: String::from(markdown_identifier),
        }
    }

    pub fn id_internal(&self) -> &str {
        &self.id
    }
}

#[Object]
impl MarkdownElement {
    pub async fn id(&self) -> ID {
        ID::from(self.id_internal())
    }

    pub async fn parent_id(&self) -> Option<&str> {
        self.parent_id.as_deref()
    }

    async fn identifier(&self) -> &str {
        &self.markdown_identifier
    }
}
