use super::{context::AppContext, elements::element_reference::ElementReference};
use crate::objects::elements::{get_element_from_reference, Element, ElementType};
use async_graphql::{Context, Object, ID};

#[derive(Clone, Debug)]
pub struct Template {
    identifier: String,
    description: String,
    root: Option<ElementReference>,
}

impl Template {
    pub fn new(identifier: &str, description: &str) -> Self {
        Template {
            identifier: String::from(identifier),
            description: String::from(description),
            root: None,
        }
    }

    pub fn identifier_internal(&self) -> &str {
        &self.identifier
    }

    pub fn set_root(&mut self, root: Element) {
        self.root = Some(match root {
            Element::HeaderElement(header) => {
                ElementReference::new(header.id_internal(), ElementType::Header)
            }
            Element::StackElement(stack) => {
                ElementReference::new(stack.id_internal(), ElementType::Stack)
            }
            Element::MarkdownElement(markdown) => {
                ElementReference::new(markdown.id_internal(), ElementType::Markdown)
            }
            Element::TableElement(_) => todo!(),
        });
    }
}

#[Object]
impl Template {
    async fn identifier(&self) -> ID {
        ID::from(&self.identifier)
    }

    async fn description(&self) -> &str {
        &self.description
    }

    async fn children<'ctx>(&self, ctx: &Context<'ctx>) -> Vec<Element> {
        let app_context = ctx.data::<AppContext>().unwrap();
        self.root
            .as_ref()
            .map(|e| {
                get_element_from_reference(e, app_context)
                    .map(|elem| elem.children(app_context))
                    .unwrap_or_default()
            })
            .unwrap_or_default()
    }
}
