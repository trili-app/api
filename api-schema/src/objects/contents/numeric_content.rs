use async_graphql::{Object, ID};

#[derive(Clone, Debug)]
pub struct NumericContent {
    id: String,
    value: f64,
}

impl NumericContent {
    pub fn new(id: &str, value: f64) -> Self {
        NumericContent {
            id: String::from(id),
            value,
        }
    }

    pub fn id_internal(&self) -> &str {
        &self.id
    }
}

#[Object]
impl NumericContent {
    async fn id(&self) -> ID {
        ID::from(&self.id)
    }

    async fn value(&self) -> f64 {
        self.value
    }
}
