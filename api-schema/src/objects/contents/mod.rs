use self::{
    date_content::DateContent, markdown_content::MarkdownContent, numeric_content::NumericContent,
    text_content::TextContent,
};
use async_graphql::{Enum, Union};

pub mod date_content;
pub mod markdown_content;
pub mod numeric_content;
pub mod text_content;

#[derive(Enum, Copy, Clone, Eq, PartialEq, Debug)]
pub enum ContentLevel {
    Beginner,
    Advanced,
    Expert,
}

#[derive(Enum, Copy, Clone, Eq, PartialEq, Debug)]
pub enum ContentType {
    Date,
    Markdown,
    Text,
    Numeric,
}

#[derive(Union)]
pub enum Content {
    MarkdownContent(MarkdownContent),
    TextContent(TextContent),
    NumericContent(NumericContent),
    DateContent(DateContent),
}
