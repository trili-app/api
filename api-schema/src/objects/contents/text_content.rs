use async_graphql::{Object, ID};

use crate::objects::language::Language;

#[derive(Clone, Debug)]
pub struct TextContent {
    identifier: String,
    text: String,
    language: Language,
}

impl TextContent {
    pub fn new(identifier: &str, language: Language, text: &str) -> Self {
        TextContent {
            identifier: String::from(identifier),
            language,
            text: String::from(text),
        }
    }

    pub fn identifier_internal(&self) -> &str {
        &self.identifier
    }

    pub fn language_internal(&self) -> &Language {
        &self.language
    }
}

#[Object]
impl TextContent {
    async fn identifier(&self) -> ID {
        ID::from(&self.identifier)
    }

    async fn text(&self) -> &str {
        &self.text
    }

    async fn language(&self) -> Language {
        self.language
    }
}
