use async_graphql::{Object, ID};

#[derive(Clone, Debug)]
pub struct MarkdownContent {
    id: String,
    beginner: Option<String>,
    advanced: Option<String>,
    expert: Option<String>,
}

impl MarkdownContent {
    pub fn new(id: &str) -> Self {
        MarkdownContent {
            id: String::from(id),
            beginner: None,
            advanced: None,
            expert: None,
        }
    }

    pub fn id_internal(&self) -> &str {
        &self.id
    }

    pub fn set_beginner(&mut self, beginner: &str) {
        self.beginner = Some(String::from(beginner));
    }

    pub fn set_advanced(&mut self, advanced: &str) {
        self.advanced = Some(String::from(advanced));
    }

    pub fn set_expert(&mut self, expert: &str) {
        self.expert = Some(String::from(expert));
    }
}

#[Object]
impl MarkdownContent {
    async fn id(&self) -> ID {
        ID::from(&self.id)
    }

    async fn beginner(&self) -> &Option<String> {
        &self.beginner
    }

    async fn advanced(&self) -> &Option<String> {
        &self.advanced
    }

    async fn expert(&self) -> &Option<String> {
        &self.expert
    }
}
