use async_graphql::{Object, ID};
use chrono::{Date, DateTime, NaiveTime, Utc};

#[derive(Clone, Debug)]
pub struct DateContent {
    id: String,
    date: Date<Utc>,
}

impl DateContent {
    pub fn new(id: &str, date: &Date<Utc>) -> Self {
        DateContent {
            id: String::from(id),
            date: *date,
        }
    }

    pub fn id_internal(&self) -> &str {
        &self.id
    }
}

#[Object]
impl DateContent {
    async fn id(&self) -> ID {
        ID::from(&self.id)
    }

    async fn date(&self) -> DateTime<Utc> {
        self.date.and_time(NaiveTime::from_hms(0, 0, 0)).unwrap()
    }
}
