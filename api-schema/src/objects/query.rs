use async_graphql::{Context, Object, ID};

use super::{context::AppContext, template::Template, topic::LocalizedTopic};
use crate::objects::topic::Topic;

pub struct QueryRoot;

#[Object]
impl QueryRoot {
    async fn topics<'ctx>(&self, ctx: &Context<'ctx>, query: String) -> Vec<Topic> {
        let app_context = ctx.data::<AppContext>().unwrap();
        let data_source = app_context.test_data_source();
        data_source.topics_by_query(query.trim(), app_context.language())
    }

    async fn templates<'ctx>(&self, ctx: &Context<'ctx>) -> Vec<Template> {
        let app_context = ctx.data::<AppContext>().unwrap();
        let data_source = app_context.test_data_source();
        data_source.templates()
    }

    async fn template<'ctx>(&self, ctx: &Context<'ctx>, id: ID) -> Option<Template> {
        let app_context = ctx.data::<AppContext>().unwrap();
        let data_source = app_context.test_data_source();
        data_source.template(&id)
    }

    async fn localized_topic<'ctx>(&self, ctx: &Context<'ctx>, id: ID) -> Option<LocalizedTopic> {
        let app_context = ctx.data::<AppContext>().unwrap();
        let data_source = app_context.test_data_source();
        data_source.localized_topic(&id)
    }
}
