use async_graphql::{Object, ID};

use super::contents::ContentType;

#[derive(Clone, Debug)]
pub struct ConnectionSchema {
    identifier: String,
    content_type: ContentType,
    unique: bool,
}

impl ConnectionSchema {
    pub fn new(identifier: &str, content_type: ContentType, unique: bool) -> Self {
        ConnectionSchema {
            identifier: String::from(identifier),
            content_type,
            unique,
        }
    }

    pub fn identifier_internal(&self) -> &str {
        &self.identifier
    }

    pub fn content_type(&self) -> ContentType {
        self.content_type
    }
}

#[Object]
impl ConnectionSchema {
    async fn identifier(&self) -> ID {
        ID::from(&self.identifier)
    }

    async fn unique(&self) -> bool {
        self.unique
    }
}
