mod examples;

use self::examples::{
    connection_schemas::generate_connection_schemas, templates::generate_templates,
    texts::generate_texts, topics::generate_topics,
};
use crate::objects::{
    connection::Connection,
    connection_schema::ConnectionSchema,
    contents::{markdown_content::MarkdownContent, text_content::TextContent},
    elements::{
        header_element::HeaderElement, markdown_element::MarkdownElement,
        stack_element::StackElement,
    },
    language::Language,
    template::Template,
    topic::{LocalizedTopic, Topic},
};
use rand::SeedableRng;
use rand_pcg::Pcg64;
use std::collections::BTreeMap;

pub struct TestDataSource {
    topics: BTreeMap<String, Topic>,
    localized_topics: BTreeMap<String, LocalizedTopic>,
    leveled_markdowns: BTreeMap<String, MarkdownContent>,
    texts: BTreeMap<String, TextContent>,
    connection_schemas: BTreeMap<String, ConnectionSchema>,
    connections: BTreeMap<String, Connection>,
    templates: BTreeMap<String, Template>,
    stacks: BTreeMap<String, StackElement>,
    headers: BTreeMap<String, HeaderElement>,
    markdowns: BTreeMap<String, MarkdownElement>,
}

impl TestDataSource {
    pub fn new() -> Self {
        let mut rng = Pcg64::seed_from_u64(123);
        let mut topics = BTreeMap::new();
        let mut localized_topics = BTreeMap::new();
        let mut leveled_markdowns = BTreeMap::new();
        let mut texts = BTreeMap::new();
        let mut connection_schemas = BTreeMap::new();
        let mut connections = BTreeMap::new();
        let mut templates = BTreeMap::new();
        let mut stacks = BTreeMap::new();
        let mut headers = BTreeMap::new();
        let mut markdowns = BTreeMap::new();

        for t in generate_connection_schemas() {
            connection_schemas.insert(String::from(t.identifier_internal()), t);
        }
        for t in generate_templates(&mut rng) {
            templates.insert(String::from(t.template.identifier_internal()), t.template);
            for s in t.stacks {
                stacks.insert(String::from(s.id_internal()), s);
            }
            for h in t.headers {
                headers.insert(String::from(h.id_internal()), h);
            }
            for h in t.markdowns {
                markdowns.insert(String::from(h.id_internal()), h);
            }
        }
        for x in generate_topics(&mut rng, templates.values().into_iter().next().unwrap()) {
            for lt in x.localized_topics {
                localized_topics.insert(String::from(lt.id_internal()), lt);
            }
            for lc in x.leveled_markdowns {
                leveled_markdowns.insert(String::from(lc.id_internal()), lc);
            }
            topics.insert(String::from(x.topic.id_internal()), x.topic);
            for conn in x.connections {
                connections.insert(String::from(conn.id_internal()), conn);
            }
        }
        for t in generate_texts() {
            texts.insert(
                TestDataSource::generate_localized_identifier(
                    t.identifier_internal(),
                    t.language_internal(),
                ),
                t,
            );
        }

        TestDataSource {
            topics,
            localized_topics,
            leveled_markdowns,
            texts,
            templates,
            connections,
            connection_schemas,
            stacks,
            headers,
            markdowns,
        }
    }

    pub fn topics_by_query(&self, query: &str, language: &Language) -> Vec<Topic> {
        self.topics
            .values()
            .filter(|t| {
                let localized_topic = t
                    .languages()
                    .get(language)
                    .or_else(|| t.languages().get(&Language::English))
                    .and_then(|id| self.localized_topics.get(id));

                localized_topic.map_or(false, |t| t.name_internal().to_lowercase().contains(query))
            })
            .cloned()
            .collect()
    }

    pub fn templates(&self) -> Vec<Template> {
        self.templates.values().cloned().collect()
    }

    pub fn topic(&self, id: &str) -> Option<Topic> {
        self.topics.get(id).cloned()
    }

    pub fn localized_topic(&self, id: &str) -> Option<LocalizedTopic> {
        self.localized_topics.get(id).cloned()
    }

    pub fn leveled_markdown(&self, id: &str) -> Option<MarkdownContent> {
        self.leveled_markdowns.get(id).cloned()
    }

    pub fn text(&self, identifier: &str, language: &Language) -> Option<TextContent> {
        self.texts
            .get(&TestDataSource::generate_localized_identifier(
                identifier, language,
            ))
            .cloned()
    }

    pub fn connection_schema(&self, id: &str) -> Option<ConnectionSchema> {
        self.connection_schemas.get(id).cloned()
    }

    pub fn connection(&self, id: &str) -> Option<Connection> {
        self.connections.get(id).cloned()
    }

    pub fn template(&self, id: &str) -> Option<Template> {
        self.templates.get(id).cloned()
    }

    pub fn header_element(&self, id: &str) -> Option<HeaderElement> {
        self.headers.get(id).cloned()
    }

    pub fn stack_element(&self, id: &str) -> Option<StackElement> {
        self.stacks.get(id).cloned()
    }

    pub fn markdown_element(&self, id: &str) -> Option<MarkdownElement> {
        self.markdowns.get(id).cloned()
    }

    fn generate_localized_identifier(identifier: &str, language: &Language) -> String {
        return format!("{}-{}", identifier, language.abbreviation());
    }
}

impl Default for TestDataSource {
    fn default() -> Self {
        Self::new()
    }
}
