use super::TopicEntry;
use crate::{
    objects::{
        connection::Connection,
        contents::{date_content::DateContent, markdown_content::MarkdownContent},
        language::Language,
        template::Template,
        topic::{LocalizedTopic, Topic},
    },
    sources::test_data::examples::generate_id,
};
use chrono::{TimeZone, Utc};
use rand_pcg::Lcg128Xsl64;
use std::collections::BTreeMap;

pub fn generate_typescript_data(rng: &mut Lcg128Xsl64, template: &Template) -> TopicEntry {
    let topic_id = generate_id(rng);
    let mut summary = MarkdownContent::new(&generate_id(rng));
    summary.set_beginner(
        "TypeScript is a programming language. \
        It's mainly used for purposes where memory safety and effiency are especially important.",
    );
    summary.set_advanced(
        "TypeScript is a systems programming language. It is mainly used for memory sensitive purposes because of it's memory allocation on build time.",
    );
    summary.set_expert("TypeScript is just awesome.");
    let summary_connection = Connection::new(&generate_id(rng), "summary", summary.id_internal());

    let topic_en = LocalizedTopic::new(
        &generate_id(rng),
        &topic_id,
        "Typescript",
        Language::English,
        &[String::from(summary_connection.id_internal())],
    );

    let mut languages = BTreeMap::new();
    languages.insert(Language::English, String::from(topic_en.id_internal()));

    let updated_content = DateContent::new(&generate_id(rng), &Utc.ymd(2020, 2, 25));
    let updated_connection =
        Connection::new(&generate_id(rng), "updated", updated_content.id_internal());

    TopicEntry {
        topic: Topic::new(
            &topic_id,
            template.identifier_internal(),
            &languages,
            &[String::from(updated_connection.id_internal())],
        ),
        localized_topics: vec![topic_en],
        leveled_markdowns: vec![summary],
        connections: vec![updated_connection, summary_connection],
        date_contents: vec![updated_content],
        numeric_contents: vec![],
    }
}
