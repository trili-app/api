use crate::{
    objects::{
        connection::Connection,
        contents::markdown_content::MarkdownContent,
        language::Language,
        template::Template,
        topic::{LocalizedTopic, Topic},
    },
    sources::test_data::examples::{generate_id, topics::TopicEntry},
};
use rand_pcg::Lcg128Xsl64;
use std::{collections::BTreeMap, vec};

pub fn generate_javascript_data(rng: &mut Lcg128Xsl64, template: &Template) -> TopicEntry {
    let topic_id = generate_id(rng);
    let mut summary = MarkdownContent::new(&generate_id(rng));
    summary.set_beginner(
        "Javascript is a programming language. \
        It's mainly used for frontend development as well as backend services.",
    );
    summary.set_advanced(
        "Javascript is a programming language. \
        It's used for frontend development and backend services using NodeJS, a single threaded runtime.",
    );
    summary.set_expert("Javascript is just awesome.");
    let summary_connection = Connection::new(&generate_id(rng), "summary", summary.id_internal());

    let topic_en = LocalizedTopic::new(
        &generate_id(rng),
        &topic_id,
        "Javascript",
        Language::English,
        &[String::from(summary_connection.id_internal())],
    );

    let mut languages = BTreeMap::new();
    languages.insert(Language::English, String::from(topic_en.id_internal()));

    let updated_connection = Connection::new(&generate_id(rng), "updated", "a");
    let incidents_connection = Connection::new(&generate_id(rng), "incidents", "a");

    TopicEntry {
        topic: Topic::new(
            &topic_id,
            template.identifier_internal(),
            &languages,
            &[
                String::from(updated_connection.id_internal()),
                String::from(incidents_connection.id_internal()),
            ],
        ),
        localized_topics: vec![topic_en],
        leveled_markdowns: vec![summary],
        connections: vec![updated_connection, incidents_connection, summary_connection],
        date_contents: vec![],
        numeric_contents: vec![],
    }
}
