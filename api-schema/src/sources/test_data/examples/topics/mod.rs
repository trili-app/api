mod javascript;
mod rust;
mod typescript;

use self::{
    javascript::generate_javascript_data, rust::generate_rust_data,
    typescript::generate_typescript_data,
};
use crate::objects::{
    connection::Connection,
    contents::{
        date_content::DateContent, markdown_content::MarkdownContent,
        numeric_content::NumericContent,
    },
    template::Template,
    topic::{LocalizedTopic, Topic},
};
use rand_pcg::Lcg128Xsl64;

pub struct TopicEntry {
    pub topic: Topic,
    pub localized_topics: Vec<LocalizedTopic>,
    pub leveled_markdowns: Vec<MarkdownContent>,
    pub connections: Vec<Connection>,
    pub date_contents: Vec<DateContent>,
    pub numeric_contents: Vec<NumericContent>,
}

pub fn generate_topics(rng: &mut Lcg128Xsl64, template: &Template) -> Vec<TopicEntry> {
    vec![
        generate_rust_data(rng, template),
        generate_javascript_data(rng, template),
        generate_typescript_data(rng, template),
    ]
}
