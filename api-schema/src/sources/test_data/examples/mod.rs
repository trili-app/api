pub mod connection_schemas;
pub mod templates;
pub mod texts;
pub mod topics;

use rand::Rng;
use rand_pcg::Lcg128Xsl64;

pub fn generate_id(rng: &mut Lcg128Xsl64) -> String {
    (rng.gen::<u16>()).to_string()
}
