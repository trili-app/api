use crate::objects::{contents::text_content::TextContent, language::Language};

pub fn generate_texts() -> Vec<TextContent> {
    vec![
        generate_summary(),
        generate_comparison(),
        generate_programming_language(),
        generate_usage(),
    ]
}

fn generate_summary() -> TextContent {
    TextContent::new("summary", Language::English, "Summary")
}

fn generate_usage() -> TextContent {
    TextContent::new("usage", Language::English, "Usage")
}

fn generate_programming_language() -> TextContent {
    TextContent::new(
        "programming-language",
        Language::English,
        "Programming Language",
    )
}

fn generate_comparison() -> TextContent {
    TextContent::new("comparison", Language::English, "Comparison")
}
