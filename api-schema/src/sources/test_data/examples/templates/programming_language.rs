use crate::{
    objects::{
        elements::{
            header_element::HeaderElement,
            markdown_element::MarkdownElement,
            stack_element::{Direction, StackElement},
            Element,
        },
        template::Template,
    },
    sources::test_data::examples::{generate_id, templates::TemplateEntry},
};
use rand_pcg::Lcg128Xsl64;

pub fn generate_programming_language_data(rng: &mut Lcg128Xsl64) -> TemplateEntry {
    let mut stack = StackElement::new(&generate_id(rng), None);
    let header_summary = HeaderElement::new(
        &generate_id(rng),
        Some(String::from(stack.id_internal())),
        "summary",
        2,
    );
    let header_usage = HeaderElement::new(
        &generate_id(rng),
        Some(String::from(stack.id_internal())),
        "usage",
        2,
    );
    let markdown_summary = MarkdownElement::new(
        &generate_id(rng),
        Some(String::from(stack.id_internal())),
        "summary",
    );
    let mut stack_vertical =
        StackElement::new(&generate_id(rng), Some(String::from(stack.id_internal())));
    let markdown_right = MarkdownElement::new(
        &generate_id(rng),
        Some(String::from(stack_vertical.id_internal())),
        "right",
    );
    let markdown_left = MarkdownElement::new(
        &generate_id(rng),
        Some(String::from(stack_vertical.id_internal())),
        "left",
    );
    stack_vertical.set_direction(Direction::Horizontal);
    stack_vertical.add_child(Element::MarkdownElement(markdown_left.clone()));
    stack_vertical.add_child(Element::MarkdownElement(markdown_right.clone()));
    let markdown_usage = MarkdownElement::new(
        &generate_id(rng),
        Some(String::from(stack.id_internal())),
        "usage",
    );
    stack.add_child(Element::HeaderElement(header_summary.clone()));
    stack.add_child(Element::MarkdownElement(markdown_summary.clone()));
    stack.add_child(Element::HeaderElement(header_usage.clone()));
    stack.add_child(Element::MarkdownElement(markdown_usage.clone()));
    stack.add_child(Element::StackElement(stack_vertical.clone()));

    let mut template = Template::new("programming-language", "Apply this template for programming languages. The boundary to more specialized languages as SQL is not defined yet. So try to stick to this template until a separate general template makes sense.");
    template.set_root(Element::StackElement(stack.clone()));

    TemplateEntry {
        template,
        headers: vec![header_usage, header_summary],
        stacks: vec![stack, stack_vertical],
        markdowns: vec![
            markdown_summary,
            markdown_usage,
            markdown_right,
            markdown_left,
        ],
    }
}
