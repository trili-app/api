mod programming_language;

use crate::{
    objects::{
        elements::{
            header_element::HeaderElement, markdown_element::MarkdownElement,
            stack_element::StackElement,
        },
        template::Template,
    },
    sources::test_data::examples::templates::programming_language::generate_programming_language_data,
};
use rand_pcg::Lcg128Xsl64;

pub struct TemplateEntry {
    pub template: Template,
    pub stacks: Vec<StackElement>,
    pub headers: Vec<HeaderElement>,
    pub markdowns: Vec<MarkdownElement>,
}

pub fn generate_templates(rng: &mut Lcg128Xsl64) -> Vec<TemplateEntry> {
    vec![generate_programming_language_data(rng)]
}
