use crate::objects::{connection_schema::ConnectionSchema, contents::ContentType};

pub fn generate_connection_schemas() -> Vec<ConnectionSchema> {
    vec![
        generate_published_data(),
        generate_updated_data(),
        generate_is_part_of_data(),
        generate_incidents_data(),
        generate_summary(),
    ]
}

fn generate_published_data() -> ConnectionSchema {
    ConnectionSchema::new("published", ContentType::Date, true)
}

fn generate_updated_data() -> ConnectionSchema {
    ConnectionSchema::new("updated", ContentType::Date, true)
}

fn generate_is_part_of_data() -> ConnectionSchema {
    ConnectionSchema::new("isPartOf", ContentType::Markdown, false)
}

fn generate_incidents_data() -> ConnectionSchema {
    ConnectionSchema::new("incidents", ContentType::Numeric, true)
}

fn generate_summary() -> ConnectionSchema {
    ConnectionSchema::new("summary", ContentType::Markdown, true)
}
