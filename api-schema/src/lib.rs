pub mod objects;
mod sources;

use async_graphql::{extensions::Logger, EmptyMutation, EmptySubscription, Schema};
use objects::{context::AppContext, query::QueryRoot};

pub fn init_schema() -> Schema<QueryRoot, EmptyMutation, EmptySubscription> {
    let context = AppContext::new();
    Schema::build(QueryRoot, EmptyMutation, EmptySubscription)
        .data(context)
        .extension(Logger)
        .finish()
}
